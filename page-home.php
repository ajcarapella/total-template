<?php /*
Template Name: Home Page
*/ 
get_template_part('includes/header'); ?>


<!-- Home Slider -->

<div id="homeCarousel" class="carousel slide">
    <div class="carousel-inner">
        <div class="item active">
            <img src="<?php echo home_url('/'); ?>wp-content/themes/total/images/mattress.jpg" alt="Slider 1"/>
        </div>
        <div class="item">
            <img src="<?php echo home_url('/'); ?>wp-content/themes/total/images/mattress.jpg" alt="Slider 2"/>
        </div>
        <div class="item">
            <img src="<?php echo home_url('/'); ?>wp-content/themes/total/images/mattress.jpg" alt="Slider 3"/>
        </div>
        
	    <ol class="carousel-indicators">
	        <li data-target="#homeCarousel" data-slide-to="0" class="active" contenteditable="false"></li>
	        <li data-target="#homeCarousel" data-slide-to="1" class="" contenteditable="false"></li>
	        <li data-target="#homeCarousel" data-slide-to="2" class="" contenteditable="false"></li>
	    </ol>
	           
    </div>    

</div>

<!-- End Carousel -->


<div class="container">
  <div class="row">

    <div class="col-xs-12 col-sm-8">
      <div id="content" role="main">
        <?php get_template_part('includes/loops/content', 'page'); ?>
      </div><!-- /#content -->
    </div>
    
    <div class="col-xs-6 col-sm-4" id="sidebar" role="navigation">
      <?php get_template_part('includes/sidebar'); ?>
    </div>
    
  </div><!-- /.row -->
</div><!-- /.container -->

<?php get_template_part('includes/footer'); ?>
