<!DOCTYPE html>
<html class="no-js">
<head>
	<title><?php wp_title('•', true, 'right'); bloginfo('name'); ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php wp_head(); ?>
	<!--<link rel="apple-touch-icon" sizes="57x57" href="<?php echo home_url('/'); ?>wp-content/themes/total/images/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo home_url('/'); ?>wp-content/themes/total/images/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo home_url('/'); ?>wp-content/themes/total/images/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo home_url('/'); ?>wp-content/themes/total/images/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo home_url('/'); ?>wp-content/themes/total/images/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo home_url('/'); ?>wp-content/themes/total/images/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo home_url('/'); ?>wp-content/themes/total/images/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo home_url('/'); ?>wp-content/themes/total/images/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo home_url('/'); ?>wp-content/themes/total/images/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo home_url('/'); ?>wp-content/themes/total/images/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo home_url('/'); ?>wp-content/themes/total/images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo home_url('/'); ?>wp-content/themes/total/images/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo home_url('/'); ?>wp-content/themes/total/images/favicon-16x16.png">
	<link rel="manifest" href="../images/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo home_url('/'); ?>wp-content/themes/total/images/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">-->
</head>

<body <?php body_class(); ?>>

<!--[if lt IE 8]>
<div class="alert alert-warning">
	You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</div>
<![endif]-->

<nav class="navbar navbar-default navbar-static-top">
<div class="container-fluid no-pad top-nav-container">
	<div class="container">
		<div class="row">
			 <div class="col-xs-12 col-sm-5 no-pad">
			 <img style="width:100%;" src="<?php echo home_url('/'); ?>/wp-content/themes/total/images/total-logo.png" alt="Logo"/>
			 </div>
		</div>
	</div>
</div>
  <div class="container-fluid no-pad bottom-nav-container">
    <div class="navbar-header">
     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
        <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar top-bar"></span>
	        <span class="icon-bar middle-bar"></span>
	        <span class="icon-bar bottom-bar"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="navbar">
             <?php //get_template_part('includes/navbar-search'); ?>
       <?php
            wp_nav_menu( array(
                'theme_location'    => 'navbar-left',
                'depth'             => 2,
                'menu_class'        => 'nav navbar-nav',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
        ?>
        
     

     </div><!-- /.navbar-collapse -->
  </div><!-- /.container -->
</nav>

<div class="overlay-nav"></div>

