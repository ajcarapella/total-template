(function ($) {
	
	"use strict";

	$(document).ready(function() {

		// Comments
		$(".commentlist li").addClass("panel panel-default");
		$(".comment-reply-link").addClass("btn btn-default");
	
		// Forms
		$('select, input[type=text], input[type=email], input[type=password], textarea').addClass('form-control');
		$('input[type=submit]').addClass('btn btn-primary');
		
		//Add Overlay To Page When Navigation is Opened
		$('.navbar-toggle').click(function(){
			$('.overlay-nav').toggleClass('show-hide');
		});
		
		//Sticky Icky
		var stickyNavTop = $('.bottom-nav-container').offset().top;
		var stickyNav = function(){
		var scrollTop = $(window).scrollTop();
		      
		if (scrollTop > stickyNavTop) { 
		    $('.bottom-nav-container').addClass('sticky');
		} else {
		    $('.bottom-nav-container').removeClass('sticky'); 
		}
		};
		 
		stickyNav();
		 
		$(window).scroll(function() {
		  stickyNav();
		});



	      
	    $('#homeCarousel').carousel({
	        interval: 4000
	    });
	

		

	});

}(jQuery));
